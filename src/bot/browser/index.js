var express = require("express");
var fs = require('fs');

var app = express();

var server = app.listen(3050, () => {console.log("Web server started on port 3050")});

let args =  {
    "content": "<h1>@botName</h1><h2>@ServerCount</h2><h2>@BotHealth</h2>",
    "botName": "Test",
    "ServerCount": 0,
    "BotHealth": "Alive",
    "servers": GetServerListHtml()
}

// app.use(express.static(__dirname + '/public'));

app.get('/', processIndex);
app.get('/server/:server/:channel?', processServer);

function processIndex(request, response) {
    let t = getHtmlFile('index',
        args
    );
    response.writeHead(200, {"Context-Type": "plain/text"});
    response.end(t);
    args["servers"] = GetServerListHtml();
}

function processServer(request,response){
    args["servers"] = GetServerListHtml();
    let data = request.params;

    let server = data.server;
    let channel = data.channel;

    let cArgs = JSON.parse(JSON.stringify(args));
    let s = GetServerList()[server];

    if(!channel){
        cArgs["content"] = "";
        Object.keys(s).forEach(function(key){
            cArgs["content"] += "<a href='/server/"+server+"/"+key+"'>"+key+"</a>";
        });
    }else{
        response.send(s);
    }

    let file = getHtmlFile('index', cArgs);

    response.writeHead(200, {"Context-Type": "plain/text"});
    response.end(file);
}

function getHtmlFile(name, parms){
    let pureHtml =  fs.readFileSync('public/' + name + '.html') + "";

    Object.keys(parms).forEach(function(key) {
        var val = parms[key];
        pureHtml = pureHtml.replace("@" + key, val);
      });

    return pureHtml;

}

function GetServerList(){
    let j = JSON.parse(fs.readFileSync(__dirname + '/../bot/data/chat.json'));
    return j;
}

function GetServerListHtml(){
    let sl = GetServerList();

    let html = "";
    Object.keys(sl).forEach(function(key) {
        html += "<li>";
        html += "<a href='/server/" + key + "'>"+key+"</a>";
        html +="<ul>";
        Object.keys(sl[key]).forEach(function(sKey){
            // html += key;
            html += "<li><a href='/server/" + key + "/" + sKey + "'>" + sKey + "</a></li>";
        });
        html +="</ul>";

        html += "</li>";
        /*var val = parms[key];
        pureHtml = pureHtml.replace("@" + key, val);*/
      });

    return html;
}